﻿<%@ Page Title="HTTP5105 Review" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTTP5105.aspx.cs" Inherits="Assignment2a_N01311709.WebForm3" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %> - Databases: SQL - Snippet of Code I Wrote</h2>
    <p>For Question #6 of Assignment #3: Advanced Joins and Aggregates, my hypothetical employer is asking me to find how many cars belong to each client and to include first and last names. To satisfy this, I wrote the following query in SQL:</p>
    
     <uctrl:CodeBox runat="server" id="exerciseQuerySQL" SkinID="CodeBox" code="SQL" owner="me" />
    
    <p>This query returns them in descending order. After being marked, I learned that I should have included and also grouped by clientid to avoid false mergers of clients.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h3>Links</h3>
    <p><a href="https://www.w3schools.com/sql/default.asp">W3C: SQL</a></p>
    <p><a href="https://docs.oracle.com/cd/B19306_01/nav/mindx.htm">Oracle Database Master Index</a></p>
    <p><a href="https://docs.oracle.com/database/121/CNCPT/sqllangu.htm#CNCPT015">Oracle Help Center: Introduction to SQL</a></p>
</asp:Content>
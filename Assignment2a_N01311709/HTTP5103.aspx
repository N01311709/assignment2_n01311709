﻿<%@ Page Title="HTTP5103 Review" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTTP5103.aspx.cs" Inherits="Assignment2a_N01311709.WebForm1" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %> - Web Programming: Javascript - Tricky Concept</h2>
    <p>I feel foggy on different function specific to arrays so I will work that out in this space.</p>
    <p>When working with arrays, <em>pop()</em> will drop the last item in the array and <em>push()</em> will append the item to the array. <em>Shift()</em> will add the item to the start of the array and <em>unshift()</em> will drop the item from the beginning of the array.</p>
    <p>Additionally, <em>sort()</em> will sort the items in the array sequentially and alphabetically.</p>
    <p>Here is an exercise I did with arrays.</p>
    
    <uctrl:CodeBox runat="server" id="exerciseGroupJS" SkinID="CodeBox" code="JS" owner="me" />
    
</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h3>Links</h3>
    <p><a href="https://www.w3schools.com/js/js_objects.asp">W3C: Objects</a></p>
    <p><a href="https://www.w3schools.com/js/js_functions.asp">W3C: Functions</a></p>
    <p><a href="https://www.w3schools.com/js/js_arrays.asp">W3C: Arrays</a></p>
    <p><a href="https://www.w3schools.com/js/js_loop_for.asp">W3C: For Loops</a></p>
    <p><a href="https://www.w3schools.com/js/js_loop_while.asp">W3C: While Loops</a></p>
    <p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions">MDN: Arrow Functions</a> (not covered; optional)</p>
</asp:Content>
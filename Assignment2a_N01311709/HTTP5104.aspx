﻿<%@ Page Title="HTTP5104 Review" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTTP5104.aspx.cs" Inherits="Assignment2a_N01311709.WebForm2" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>: Digital Design: CSS - Snippet of Code I Found</h2>
    <p>I have not really used snippets of code for just about any class except for custom validators in this class. However, the following is CSS related.</p>
    <p>For this assignment, I was wondering how to best format the block quotes of code in CSS. I looked at W3 Schools and how they did it for some guidance. Their style rules are copious, but what was most useful was as follows:</p>
    <div class="code-block">
    <img src="a3_image.PNG" />
    </div>
    <p>I did not want to type this out because it would take too long. However, I tried cribbed a bit from W3C on how they present code, particularly the typeface. I also did a lot of strategic Googling for the image sizing above.</p>

</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h3>Links</h3>
    <p><a href="https://validator.w3.org/">W3C HTML Validator</a></p>
    <p><a href="https://jigsaw.w3.org/css-validator/">W3C CSS Validator</a></p>
    <p><a href="https://fonts.google.com/">Google Fonts</a></p>
</asp:Content>

﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a_N01311709._Default" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2>Assignment 2b</h2>
    <p>Welcome. Stay a while. Have a look around.</p>
    <p>Update for 2b: The CodeBox user control and implementation of the DataGrid for code will be visible on the Javascript and SQL review pages.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h3>Links</h3>
    <p>Each of the review pages will have relevant links to the topic posted in the column on the right.</p>
</asp:Content>

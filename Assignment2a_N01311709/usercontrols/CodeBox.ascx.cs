﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2a_N01311709
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string Code
        {
            get { return (string)ViewState["Code"]; }
            set { ViewState["Code"] = value; }
        }

        DataView CreateCodeSource()
        {
            DataTable mycode = new DataTable();
            DataColumn idx_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            mycode.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            mycode.Columns.Add(code_col);

            List<string> my_sample_code = new List<string>();

            if (Code == "SQL") {
                List<string> my_sql_sample = new List<string>(new string[] {
                "select clientfname || ' ' || clientlname as \"Name\", count(carsxclients.carid) as \"Number of Cars\"",
                "~from clients",
                "~~inner join carsxclients",
                "~~~on clients.clientid = carsxclients.clientid",
                "~~inner join cars",
                "~~~on carsxclients.carid = cars.carid",
                "~group by clientid, clientfname, clientlname",
                "~order by count(carsxclients.carid) desc;"
            });

                my_sample_code = my_sql_sample;

            } else if (Code == "JS") {
                List<string> my_js_sample = new List<string>(new string[] {
                "//PM TEAM MEMBER ARRAY",
                "var ourTeam = [\"Delan\", \"Imzan\", \"Twen\", \"Larry\", \"Ridham\"];",
                "",
                "//OUTPUT TEAM ARRAY TO JS CONSOLE",
                "",
                "console.log(ourTeam);",
                "",
                "//REMOVE LAST MEMBER",
                "ourTeam.pop();",
                "console.log(ourTeam);",
                "",
                "//ADD SEAN TO FRONT OF ARRAY",
                "ourTeam.unshift(\"Sean\");",
                "console.log(ourTeam);",
                "",
                "//REARRANGE THE ARRAY ALPHABETICALLY",
                "var teamAlpha = ourTeam.sort();",
                "console.log(ourTeam);",
                "",
                "//OUTPUT REQUIRED MESSAGE TO JS CONSOLE",
                "console.log(\"We have \" + ourTeam.length + \" people in our group\");",
                "",
                "//LOOP THROUGH ARRAY TO OUTPUT TEAM MEMBERS/NUMBERS TO JS CONSOLE",
                "for (var i = 0; i < ourTeam.length; i++)",
                "~{",
                "~~console.log(ourTeam[i] + \" is \" + (i + 1));</ p >",
                "~}"
            });
                my_sample_code = my_js_sample;
            };


            int i = 1;
            foreach (string line in my_sample_code)
            {
                coderow = mycode.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                mycode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(mycode);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            myCode.DataSource = ds;
            myCode.DataBind();
        }
    }
}
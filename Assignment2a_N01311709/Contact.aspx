﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Assignment2a_N01311709.Contact" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
<p>d.hamasoor@gmail.com</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h3>Links</h3>
    <p><a href="mailto:d.hamasoor@gmail.com">Click here to send me an email.</a></p>
</asp:Content>

﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Assignment2a_N01311709.About" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p>The CodeBox user control and implementation of the DataGrid for code will be visible on the Javascript and SQL review pages.</p>
    <p>The data grids, skins, themes, and codebox user controls and logic on display here are intended to satisfy the requirements for Assignment 2b in HTTP5101: Web Application Development for Professor Christine Bittle.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h3>Links</h3>
    <p><a href="https://bitbucket.org/N01311709/">My Bitbucket profile</a></p>
</asp:Content>